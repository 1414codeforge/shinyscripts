Shiny Scripts
=============

Old fashioned collection of shell scripts to init Arch using [SysVinit](https://wiki.archlinux.org/title/SysVinit).

> WARNING:
> Arch does not officially support using any init system other than [systemd](https://wiki.archlinux.org/title/Systemd).

## Why?

Because I like it, and because Linux is about choice.

## Acknowledgement

This is a fork of https://bitbucket.org/TZ86/initscripts-fork.

## License

MIT, see [LICENSE](LICENSE) file for details.